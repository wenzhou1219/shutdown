#include <Windows.h>
#include "shutdown.h"

#pragma comment(lib, "Advapi32")										//提升权限函数
#pragma comment(lib, "User32")											//ExitWindowsEx函数

/************************************************************************/
/* 
** 功能：执行关机前的预处理，主要是提升当前进程（本关机程序）的权限
*/
/************************************************************************/
BOOL pre_process()  
{   
	HANDLE hToken;														//进程令牌句柄
	TOKEN_PRIVILEGES tkp;												//进程令牌权限结构			

	// 打开当前进程令牌  
	if(OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken) == 0)  
	{  
		return FALSE;  
	}  

	// 查询SE_SHUTDOWN_NAME权限ID  
	if(LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, &tkp.Privileges[0].Luid) == 0)  
	{  
		return FALSE;  
	}  

	tkp.PrivilegeCount = 1;												//待提升的权限数目
	tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;				//权限设置为使能

	// 提升至SE_SHUTDOWN_NAME权限  
	if(AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, (PTOKEN_PRIVILEGES)NULL, 0) == 0)  
	{  
		return FALSE;  
	}  

	//确信没有错误
	if (GetLastError() != ERROR_SUCCESS)   
		return FALSE;  

	return TRUE;  
}  


/************************************************************************/
/* 
** 功能:对输入的不同关机任务参数，执行不同操作
** 输入:预定义的关机任务参数（宏）
*/
/************************************************************************/
BOOL shut_down(int iFlag)
{
	//保证关机前准备工作完成
	if(FALSE == pre_process())
	{
		return FALSE ;
	}

	//不同关机任务，不同操作
	switch(iFlag)
	{
	case SHUTDOWN:
		if(ExitWindowsEx(EWX_SHUTDOWN | EWX_FORCE, SHTDN_REASON_FLAG_PLANNED) == 0)  
		{
			return FALSE;  
		}
		break ;
	case RESTART:
		if(ExitWindowsEx(EWX_REBOOT | EWX_FORCE, SHTDN_REASON_FLAG_PLANNED) == 0)  
		{
			return FALSE;  
		}
		break ;
	case LOGOFF:
		if(ExitWindowsEx(EWX_LOGOFF | EWX_FORCE, SHTDN_REASON_FLAG_PLANNED) == 0)  
		{
			return FALSE;  
		}
		break ;
	}
	
	return TRUE;  
}