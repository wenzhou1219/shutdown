//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Shutdown.rc
//
#define IDI_APP                         1
#define IDD_MAIN                        101
#define IDD_MAIN1                       102
#define IDD_SETTING                     102
#define IDD_ABOUT                       107
#define IDB_SET                         1000
#define IDT_TIME                        1001
#define IDC_HOUR                        1002
#define IDC_MINUTE                      1003
#define IDC_SECOND                      1005
#define IDC_WEEKDAY                     1006
#define IDC_TIMEMODEL                   1007
#define IDC_TIMEOPERATE                 1008
#define IDB_ADD                         1009
#define IDB_HIDE                        1010
#define IDB_TRAY                        1011
#define IDB_DELETE                      1012
#define IDC_DATE                        1013
#define IDC_TIME                        1014
#define IDE_CURTIME                     1015
#define IDL_TIMELIST                    1016
#define IDB_EXIT                        1017
#define IDH_HOTKEY                      1018
#define IDC_AUTORUN                     1019
#define IDC_HIDE                        1020
#define IDC_SYSLINK1                    1021
#define IDC_SYSLINK                     1021
#define IDB_SYSLINK                     1021
#define IDB_ABOUT                       1022

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
