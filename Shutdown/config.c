#include <Windows.h>
#include <stdio.h>
#include <WindowsX.h>
#include <tchar.h>
#include "config.h"
#include "shutdown.h"

//全局维护的定时列表和定时个数,其作用域限制在本文件内
static TIMENODE timeList[MAX_COUNT];
static int timeCount=0;

//软件行为配置文件
BOOL g_bIsHide		= TRUE;							//是否隐藏到后台
BOOL g_bIsAutoRun	= TRUE;							//是否自动启动
UINT g_nModifier	= MOD_ALT;						//热键转换
UINT g_nKey			= (UINT)'S';					//热键键值

/**
 *功能:将当前的定时列表写到配置文件中
 *参数:szConfigFile--参数文件路径
 *返回:设置成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL SetConfig(LPCTSTR szConfigFile)
{
	FILE* fp ;
	int i;

	//打开文件
	if (NULL == (fp = _tfopen(szConfigFile, TEXT("w"))))
	{
		MessageBox(NULL, TEXT("当前保存定时列表配置到文件出错!"), TEXT("保存定时列表配置"), MB_OK | MB_ICONHAND) ;
		return FALSE ;
	}

	//写入设置
	for (i = 0; i<timeCount; i++)
	{
		_ftprintf(fp, 
				  TEXT("TIME:%d %dTIMEMODEL:%dTIMEOPERATE:%d\n"),
				  timeList[i].time.HighPart, 
				  timeList[i].time.LowPart,
				  timeList[i].model,
				  timeList[i].operate);
	}

	fclose(fp) ;
	return TRUE ;
}

/**
 *功能:将当前的软件设置列表写到配置文件中
 *参数:szConfigFile--参数文件路径
 *返回:设置成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL SetSoftwareConfig(LPCTSTR szConfigFile)
{
	FILE* fp ;
	int i;

	//打开文件
	if (NULL == (fp = _tfopen(szConfigFile, TEXT("w"))))
	{
		MessageBox(NULL, TEXT("当前保存软件配置到文件出错!"), TEXT("保存软件配置"), MB_OK | MB_ICONHAND) ;
		return FALSE ;
	}

	//写入设置
		_ftprintf(fp, 
			TEXT("HIDE:%d AUTORUN:%d MODIFIER:%d VK:%d\n"),
				  g_bIsHide, 
				  g_bIsAutoRun,
				  g_nModifier,
				  g_nKey);

	fclose(fp) ;
	return TRUE ;
}

/**
 *功能:从定时列表配置文件中读取参数到当前的定时列表
 *参数:szConfigFile--参数文件路径
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL ReadConfig(LPCTSTR szConfigFile)
{
	FILE* fp ;

	//打开文件
	if (NULL == (fp = _tfopen(szConfigFile, TEXT("r"))))
	{
		MessageBox(NULL, TEXT("读入定时列表配置文件出错!"), TEXT("读入定时列表配置"), MB_OK | MB_ICONHAND) ;
		return FALSE ;
	}

	//读入设置
	timeCount = 0;
	while( !feof(fp) )
	{
		_ftscanf(fp, 
				 TEXT("TIME:%d %dTIMEMODEL:%dTIMEOPERATE:%d\n"),
				 &timeList[timeCount].time.HighPart, 
				 &timeList[timeCount].time.LowPart,
				 &timeList[timeCount].model,
				 &timeList[timeCount].operate);

		if (timeList[timeCount].time.HighPart == 0 && timeList[timeCount].time.LowPart==0)
		{
			break;
		}
		else
		{
			timeCount++;
		}
	}

	fclose(fp) ;
	return TRUE ;
}

/**
 *功能:从软件配置文件中读取参数到当前的对应参数中
 *参数:szConfigFile--参数文件路径
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL ReadSoftwareConfig(LPCTSTR szConfigFile)
{
	FILE* fp ;

	//打开文件
	if (NULL == (fp = _tfopen(szConfigFile, TEXT("r"))))
	{
		MessageBox(NULL, TEXT("读入软件配置文件出错!"), TEXT("读入软件配置"), MB_OK | MB_ICONHAND) ;
		return FALSE ;
	}

	//读入设置
		_ftscanf(fp, 
			TEXT("HIDE:%d AUTORUN:%d MODIFIER:%d VK:%d\n"),
			&g_bIsHide, 
			&g_bIsAutoRun,
			&g_nModifier,
			&g_nKey);

	fclose(fp) ;
	return TRUE ;
}

/**
 *功能:在当前的定时列表添加一个定时数据,按照时间大小从小往大排列
 *参数:time--64位时间参数
       timeModel--定时模式
	   timeOperate--定时操作
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL AddTimeNode(const ULARGE_INTEGER time, const TIMEMODEL timeModel, const TIMEOPERATE timeOperate)
{
	int i, iInsert;

	//当前已经达到定时个数上限
	if (timeCount == MAX_COUNT)
	{
		return FALSE;
	}
	
	//顺序查找按序插入,从前往后查找，时间越靠后的序号越大
	iInsert = 0;
	while (time.QuadPart>=timeList[iInsert].time.QuadPart && iInsert<=(timeCount-1))
	{
		iInsert++;
	}

	//顺序后移数据
	for (i= timeCount; i > iInsert; i--)
	{
		timeList[i] = timeList[i-1];
	}

	//插入数据
	timeList[iInsert].time = time;
	timeList[iInsert].model = timeModel;
	timeList[iInsert].operate = timeOperate;
	timeCount++;
	
	return TRUE ;
}

/**
 *功能:在当前的定时列表删除指定序号的定时数据
 *参数:nIndex--定时序号
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL RemoveTimeNode(const UINT nIndex)
{
	int i, iInsert;

	//输入索引合法性检查
	if (timeCount == 0 || nIndex < 0 || nIndex>(timeCount-1))
	{
		return FALSE;
	}

	//删除数据
	for (i= nIndex; i < timeCount-1; i++)
	{
		timeList[i] = timeList[i+1];
	}
	timeCount--;
	
	return TRUE ;
}

/**
 *功能:判断当前定时列表中是否包含指定的时间
 *参数:st--时间
 *返回:定时列表中的序号，从0开始，如果没有找到则返回-1
 *其他:2014/01/15 By Jim Wen Ver1.0
**/
int IsOnTime(const SYSTEMTIME st)
{
	FILETIME ft;
	ULARGE_INTEGER lnTime;
	int i;

	SystemTimeToFileTime(&st, &ft);
	lnTime.HighPart = ft.dwHighDateTime;
	lnTime.LowPart = ft.dwLowDateTime;

	for (i=0; i<timeCount; i++)
	{
		if ((lnTime.QuadPart)/10000000 == (timeList[i].time.QuadPart)/10000000)//FILETIME单位为100ns，太精确，直接截取判断
		{
			return i;
		}
	}

	return -1;
}

/**
 *功能:定时到达后的操作,不同模式更新下一次操作和相关参数及参数文件，不同操作下不同的操作
 *参数:nIndex--定时列表序号
	   hCtrl--列表框句柄
	   szConfigFile--配置文件路径
 *返回:无
 *其他:2014/01/15 By Jim Wen Ver1.0
**/
void OperateOnTime(const int nIndex, const HWND hCtrl, LPCTSTR szConfigFile)
{
	TIMENODE tmNode = timeList[nIndex];

	//不同模式更新定时列表
	switch (tmNode.model)
	{
	case TIMEMODEL_ONCE:
		RemoveTimeNode(nIndex);
		break;
	case TIMEMODEL_EVERYDAY:
		timeList[nIndex].time.QuadPart += 24*60*60*((ULONGLONG)10000000);//FILETIME单位为100ns,同时注意整形常量溢出的情况，必须先将常量转换类型
		break;
	case TIMEMODEL_EVERYWEEK:
		timeList[nIndex].time.QuadPart += 7*24*60*60*((ULONGLONG)10000000);
		break;
	}

	//更新列表显示
	ShowToListbox(hCtrl);

	//保存到文件
	SetConfig(szConfigFile);

	//不同模式对应不同操作
	switch(tmNode.operate)
	{
	case TIMEOPERATE_SHUTDOWN:
		shut_down(SHUTDOWN);
		break;
	case TIMEOPERATE_RESTART:
		shut_down(RESTART);
		break;
	case TIMEOPERATE_LOGOFF:		
		shut_down(LOGOFF);
		break;
	}
}

/**
 *功能:将当前定时列表内容显示到列表框中
 *参数:hCtrl--列表框句柄
 *返回:无
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
void ShowToListbox(const HWND hCtrl)
{
	int			i;
	TCHAR		szBuffer[256];
	TCHAR		szModel[256], szOperate[256];
	SYSTEMTIME	st;
	FILETIME	ft;

	//先清空之前的数据
	ListBox_ResetContent(hCtrl);

	//当前定时列表数据显示
	for (i=0; i<timeCount; i++)
	{
		ft.dwHighDateTime = timeList[i].time.HighPart;
		ft.dwLowDateTime = timeList[i].time.LowPart;
		FileTimeToSystemTime(&ft, &st);

		switch (timeList[i].model)
		{
		case TIMEMODEL_ONCE:
			lstrcpy(szModel, TEXT("仅此一次"));
			break;
		case TIMEMODEL_EVERYDAY:
			lstrcpy(szModel, TEXT("每天"));
			break;
		case TIMEMODEL_EVERYWEEK:
			lstrcpy(szModel, TEXT("每个星期"));
			break;
		}

		switch (timeList[i].operate)
		{
		case TIMEOPERATE_SHUTDOWN:
			lstrcpy(szOperate, TEXT("关机"));
			break;
		case TIMEOPERATE_RESTART:
			lstrcpy(szOperate, TEXT("重启"));
			break;
		case TIMEOPERATE_LOGOFF:
			lstrcpy(szOperate, TEXT("注销"));
			break;
		}

		_stprintf(szBuffer, 
			      TEXT("%04d年%02d月%02d日%02d时%02d分%02d秒 %s %s"),
				  st.wYear,
				  st.wMonth,
				  st.wDay,
				  st.wHour,
				  st.wMinute,
				  st.wSecond,
				  szOperate,
				  szModel);

		ListBox_InsertString(hCtrl, i, szBuffer);
		ListBox_SetItemHeight(hCtrl, i, 30);
	}
}