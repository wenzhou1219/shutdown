#include <Windows.h>
#include <ShellAPI.h>
#include "trayicon.h"

static NOTIFYICONDATA nid;//作用域仅限本文件
static BOOL	bIsToTray=FALSE;//是否已经托盘化，默认没有托盘化

/**
 *功能:显示托盘图标
 *参数:	hCtrl			窗口句柄
		uIconID			托盘图标ID
 *返回:成功TRUE，否则FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL ToTray(const HWND hCtrl, const UINT uIconID)
{
	if (TRUE == bIsToTray)
	{
		return TRUE;
	}

	//设置托盘设置结构体参数
	nid.cbSize				= sizeof(NOTIFYICONDATA);								
	nid.hWnd				= hCtrl;													
	nid.uID					= uIconID;													
	nid.hIcon				= LoadIcon((HINSTANCE)GetWindowLong(hCtrl, GWL_HINSTANCE),
										MAKEINTRESOURCE(uIconID));
	nid.uFlags				= NIF_ICON|NIF_TIP|NIF_MESSAGE;					
	nid.uCallbackMessage	= WM_TRAYMESSAGE;										
	lstrcpy(nid.szTip, TEXT("左键双击显示当前定时列表\n右键显示菜单"));				

	//在托盘区添加图标
	if(Shell_NotifyIcon(NIM_ADD,&nid)) 									
	{
		bIsToTray = TRUE;

		return TRUE ;
	}
	else
	{
		MessageBox(hCtrl, TEXT("创建托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}

/**
 *功能:删除托盘图标
 *参数:	hCtrl			窗口句柄
 *返回:成功TRUE，否则FALSE
 *其他:2014/01/12 By Jim Wen Ver1.0
**/
BOOL DeleteTray(const HWND hCtrl)
{
	if (FALSE == bIsToTray)
	{
		return TRUE;
	}

	//删除托盘图标
	if(Shell_NotifyIcon(NIM_DELETE, &nid)) 									
	{
		bIsToTray = FALSE;
		return TRUE ;
	}
	else
	{
		MessageBox(hCtrl, TEXT("删除托盘图标失败"), TEXT("错误"), MB_OK) ;
		return FALSE ;
	}
}